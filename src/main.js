import Vue from 'vue'
import App from './App.vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faClosedCaptioning, faCompressAlt, faExpandAlt, faPlay, faPause, faVolumeDown, faVolumeMute, faVolumeUp } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import './assets/css/index.css'

library.add(faClosedCaptioning, faCompressAlt, faExpandAlt, faPlay, faPause, faVolumeDown, faVolumeMute, faVolumeUp)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
